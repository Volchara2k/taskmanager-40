<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<html>

    <head>
        <title>Task Manager</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style type="text/css"><%@include file="../resources/styles.css"%></style>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </head>

    <body>
        <div class="container-content">
            <table class="list-table" height="100%" >
                <header>
                    <tr>
                        <ul>
                          <li><a href="${pageContext.request.contextPath}/">Task Manager</a></li>
                          <li><a href="${pageContext.request.contextPath}/tasks/">Tasks</a></li>
                          <li><a href="${pageContext.request.contextPath}/projects/">Projects</a></li>
                        </ul>
                    </tr>
                </header>
                <main>
                    <tr>
                        <td height="100%" valign="top" style="padding: 5%;">