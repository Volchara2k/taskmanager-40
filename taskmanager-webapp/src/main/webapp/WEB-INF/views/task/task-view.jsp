<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<jsp:include page="../../include/_header.jsp" />

<div class="list_header-content">
    <h2>View task: &laquo;${task.title}&raquo;</h2>
</div>

<div class="main-content">

    <table>
        <tr>
            <th width="200" nowrap="nowrap">ID</th>
            <th width="200" nowrap="nowrap">TITLE</th>
            <th width="200">PROJECT</th>
            <th width="100%">DESCRIPTION</th>
            <th width="150" nowrap="nowrap">STATUS</th>
            <th width="200">CREATION DATE</th>
            <th width="150">START DATE</th>
            <th width="150">END DATE</th>
        </tr>
            <tr>
                <td>
                    <c:out value="${task.id}" />
                </td>

                <td>
                    <c:out value="${task.title}" />
                </td>

                <td>
                    <c:out value="${task.project.title}" />
                </td>

                <td>
                    <c:out value="${task.description}"/>
                </td>

                <td>
                    <c:out value="${task.status.title}" />
                </td>

                <td>
                    <fmt:formatDate value="${task.timeFrame.creationDate}" pattern="dd.MM.yyyy" />
                </td>

                <td>
                    <fmt:formatDate value="${task.timeFrame.startDate}" pattern="dd.MM.yyyy" />
                </td>

                <td>
                    <fmt:formatDate value="${task.timeFrame.endDate}" pattern="dd.MM.yyyy" />
                </td>
            </tr>
    </table>

    <div class="task_action-content" style="display: inline-block; float: right;">
        <a href="/task/edit/${task.id}"><button class="btn btn-outline-primary btn-sm">Edit task</button></a>
        <a href="/task/delete/${task.id}"><button class="btn btn-outline-danger btn-sm">Delete task</button></a>
    </div>

</div>

<jsp:include page="../../include/_footer.jsp" />