package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.model.Task;

public interface ITaskUserService extends IOwnerUserService<Task> {

    @NotNull
    Task addOwnerUser(
            @Nullable Task task
    );

}