package ru.renessans.jvschool.volkov.task.manager.configuration;

import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.WebApplicationInitializer;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.soap.IProjectSoapEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.soap.ITaskSoapEndpoint;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;
import javax.xml.ws.Endpoint;

@Configuration
@ComponentScan(basePackages = "ru.renessans.jvschool.volkov.task.manager.endpoint.soap")
public class WebSoapConfiguration implements WebApplicationInitializer {

    @Override
    public void onStartup(
            @NotNull final ServletContext servletContext
    ) {
        @NotNull final CXFServlet soapServlet = new CXFServlet();
        @NotNull final ServletRegistration.Dynamic dispatcher =
                servletContext.addServlet("soapServlet", soapServlet);
        dispatcher.addMapping("/ws/*");
        dispatcher.setLoadOnStartup(1);
    }

    @Bean
    @NotNull
    public SpringBus cxf() {
        return new SpringBus();
    }

    @Bean
    @NotNull
    public Endpoint projectEndpointRegistry(
            @NotNull final IProjectSoapEndpoint projectSoapEndpoint,
            @NotNull final SpringBus cxf
    ) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, projectSoapEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }

    @Bean
    @NotNull
    public Endpoint taskEndpointRegistry(
            @NotNull final ITaskSoapEndpoint taskSoapEndpoint,
            @NotNull final SpringBus cxf
    ) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, taskSoapEndpoint);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }

}