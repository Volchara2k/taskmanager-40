
# TimeFrameDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**creationDate** | [**OffsetDateTime**](OffsetDateTime.md) | Date the task was created | 
**startDate** | [**OffsetDateTime**](OffsetDateTime.md) | Date the task was started |  [optional]
**endDate** | [**OffsetDateTime**](OffsetDateTime.md) | Date the task was ended |  [optional]



