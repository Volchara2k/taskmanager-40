#!/usr/bin/env bash

echo "Attempting to shutdown the task server..."

PORT="8080"
if [ -n "$1" ]; then
  PORT=$1
fi
FILE_PID="$PORT.pid"

echo  "Shutdown server at $PORT...";
if [ ! -f ./"$FILE_PID" ]; then
  echo "Task manager server pid not found!"
  exit 1
fi

# shellcheck disable=SC2046
kill -9 $(cat ./"$FILE_PID")
echo "Process with pid '$(cat ./"$FILE_PID")' was killed!"
rm ./"$FILE_PID"
echo "It's okay!"