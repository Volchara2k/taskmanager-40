package ru.renessans.jvschool.volkov.task.manager.listener.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.Status;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.TaskSoapEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.TimeFrameDTO;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalInputEvent;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.ws.Holder;
import java.util.UUID;

@Component
public class TaskUpdateListener extends AbstractTaskListener {

    @NotNull
    private static final String CMD_TASK_UPDATE_BY_ID = "task-update-by-id";

    @NotNull
    private static final String DESC_TASK_UPDATE_BY_ID = "обновить задачу по идентификатору";

    @NotNull
    private static final String NOTIFY_TASK_UPDATE_BY_ID =
            "Происходит попытка инициализации обновления задачи. \n" +
                    "Для обновления задачи по идентификатору введите идентификатор задачи из списка.\n" +
                    "Для обновления задачи введите её заголовок или заголовок с описанием. ";

    public TaskUpdateListener(
            @NotNull final TaskSoapEndpoint taskEndpoint
    ) {
        super(taskEndpoint);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_TASK_UPDATE_BY_ID;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_TASK_UPDATE_BY_ID;
    }

    @Async
    @Override
    @SneakyThrows
    @EventListener(condition = "@taskUpdateListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalInputEvent terminalEvent) {
        ViewUtil.print(NOTIFY_TASK_UPDATE_BY_ID);
        @NotNull final String id = ViewUtil.getLine();
        @NotNull final String title = ViewUtil.getLine();
        @NotNull final String description = ViewUtil.getLine();

        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(id);
        taskDTO.setUserId(UUID.randomUUID().toString());
        taskDTO.setTitle(title);
        taskDTO.setDescription(description);
        taskDTO.setStatus(Status.NOT_STARTED);
        @NotNull final TimeFrameDTO timeFrameDTO = new TimeFrameDTO();
        timeFrameDTO.setCreationDate(DatatypeFactory.newInstance().newXMLGregorianCalendar());
        taskDTO.setTimeFrame(timeFrameDTO);

        @NotNull final Holder<TaskDTO> dtoHolder = new Holder<>(taskDTO);
        super.taskEndpoint.updateTask(dtoHolder);
    }

}