package ru.renessans.jvschool.volkov.task.manager.listener.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.ProjectSoapEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.Status;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.TimeFrameDTO;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalInputEvent;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.ws.Holder;
import java.util.UUID;

@Component
public class ProjectUpdateListener extends AbstractProjectListener {

    @NotNull
    private static final String CMD_PROJECT_UPDATE_BY_ID = "project-update-by-id";

    @NotNull
    private static final String DESC_PROJECT_UPDATE_BY_ID = "обновить проект по идентификатору";

    @NotNull
    private static final String NOTIFY_PROJECT_UPDATE_BY_ID =
            "Происходит попытка инициализации обновления проекта. \n" +
                    "Для обновления проекта по идентификатору введите идентификатор проекта из списка. \n " +
                    "Для обновления проекта введите его заголовок или заголовок с описанием. ";

    public ProjectUpdateListener(
            @NotNull final ProjectSoapEndpoint projectEndpoint
    ) {
        super(projectEndpoint);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_PROJECT_UPDATE_BY_ID;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_PROJECT_UPDATE_BY_ID;
    }

    @Async
    @Override
    @SneakyThrows
    @EventListener(condition = "@projectUpdateListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalInputEvent terminalEvent) {
        ViewUtil.print(NOTIFY_PROJECT_UPDATE_BY_ID);
        @NotNull final String id = ViewUtil.getLine();
        @NotNull final String title = ViewUtil.getLine();
        @NotNull final String description = ViewUtil.getLine();

        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId(id);
        projectDTO.setUserId(UUID.randomUUID().toString());
        projectDTO.setTitle(title);
        projectDTO.setDescription(description);
        projectDTO.setStatus(Status.NOT_STARTED);
        @NotNull final TimeFrameDTO timeFrameDTO = new TimeFrameDTO();
        timeFrameDTO.setCreationDate(DatatypeFactory.newInstance().newXMLGregorianCalendar());
        projectDTO.setTimeFrame(timeFrameDTO);

        @NotNull final Holder<ProjectDTO> dtoHolder = new Holder<>(projectDTO);
        super.projectEndpoint.addProject(dtoHolder);
    }

}