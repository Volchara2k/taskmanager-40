package ru.renessans.jvschool.volkov.task.manager.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.TaskSoapEndpoint;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalInputEvent;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

import java.util.Collection;

@Component
public class TaskListListener extends AbstractTaskListener {

    @NotNull
    private static final String CMD_TASK_LIST = "task-list";

    @NotNull
    private static final String DESC_TASK_LIST = "вывод списка задач";

    @NotNull
    private static final String NOTIFY_TASK_LIST = "Текущий список задач: ";

    public TaskListListener(
            @NotNull final TaskSoapEndpoint taskEndpoint
    ) {
        super(taskEndpoint);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_TASK_LIST;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_TASK_LIST;
    }

    @Async
    @Override
    @EventListener(condition = "@taskListListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalInputEvent terminalEvent) {
        ViewUtil.print(NOTIFY_TASK_LIST);
        @Nullable final Collection<TaskDTO> tasks = super.taskEndpoint.getAllTasks();
        ViewUtil.print(tasks);
    }

}