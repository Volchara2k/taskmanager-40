package ru.renessans.jvschool.volkov.task.manager.endpoint;

import junitparams.JUnitParamsRunner;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;

import java.util.UUID;

@RunWith(value = JUnitParamsRunner.class)
@Category(IntegrationImplementation.class)
public final class AuthenticationEndpointTest {

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();


    @NotNull
    private final AdminEndpointService adminEndpointService = new AdminEndpointService();

    @NotNull
    private final AdminEndpoint adminEndpoint = adminEndpointService.getAdminEndpointPort();


    @NotNull
    private final AuthenticationEndpointService authenticationEndpointService = new AuthenticationEndpointService();

    @NotNull
    private final AuthenticationEndpoint authEndpoint = authenticationEndpointService.getAuthenticationEndpointPort();

    @Test
    @TestCaseName("Run testSignUpUser for signUpUser(null, random, random)")
    public void testSignUpUser() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);

        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addUser = this.authEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addUser);
        Assert.assertEquals(addUser.getId(), addUser.getId());
        Assert.assertEquals(login, addUser.getLogin());
        @NotNull final SessionDTO open = sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        final int deleteFlag = this.adminEndpoint.deleteUserByLogin(open, login);
        Assert.assertEquals(1, deleteFlag);
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

}