package ru.renessans.jvschool.volkov.task.manager.util;

import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.UtilityImplementation;

public final class SystemUtilTest {

    @Test
    @TestCaseName("Run testGetFormatData for getHardwareData()")
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    public void testGetFormatData() {
        @NotNull final String data = SystemUtil.getHardwareDataAsString();
        Assert.assertNotNull(data);
    }

}