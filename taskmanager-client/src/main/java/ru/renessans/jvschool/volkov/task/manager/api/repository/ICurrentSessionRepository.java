package ru.renessans.jvschool.volkov.task.manager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;

public interface ICurrentSessionRepository {

    @NotNull
    SessionDTO set(@NotNull SessionDTO sessionDTO);

    @Nullable
    SessionDTO get();

    @NotNull
    SessionDTO delete();

}