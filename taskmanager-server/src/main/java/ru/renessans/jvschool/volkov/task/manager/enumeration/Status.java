package ru.renessans.jvschool.volkov.task.manager.enumeration;

import org.jetbrains.annotations.NotNull;

public enum Status {

    @NotNull
    NOT_STARTED("Not started"),

    @NotNull
    IN_PROGRESS("In progress"),

    @NotNull
    COMPLETED("Completed");

    @NotNull
    private final String title;

    Status(@NotNull final String title) {
        this.title = title;
    }

    @NotNull
    public String getTitle() {
        return this.title;
    }

}