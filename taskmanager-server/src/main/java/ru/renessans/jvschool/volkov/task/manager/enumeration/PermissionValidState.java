package ru.renessans.jvschool.volkov.task.manager.enumeration;

import org.jetbrains.annotations.NotNull;

public enum PermissionValidState {

    @NotNull
    SUCCESS("Успешно!"),

    @NotNull
    LOGGED("Вы уже авторизованы!"),

    @NotNull
    NEED_LOG_IN("Необходимо авторизоваться!"),

    @NotNull
    NO_ACCESS_RIGHTS("Нет прав доступа!");

    @NotNull
    private final String title;

    PermissionValidState(@NotNull final String title) {
        this.title = title;
    }

    @NotNull
    public String getTitle() {
        return this.title;
    }

    public boolean isNotSuccess() {
        return this != SUCCESS;
    }

}