package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.dto.DomainDTO;

public interface IDomainService {

    DomainDTO dataImport(@Nullable DomainDTO domain);

    DomainDTO dataExport(@Nullable DomainDTO domain);

}