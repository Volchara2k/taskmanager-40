package ru.renessans.jvschool.volkov.task.manager.exception.illegal;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class IllegalIndexException extends AbstractException {

    @NotNull
    private static final String INDEX_ILLEGAL = "Ошибка! Параметр \"индекс\" является нелегальным!\n";

    public IllegalIndexException() {
        super(INDEX_ILLEGAL);
    }

}